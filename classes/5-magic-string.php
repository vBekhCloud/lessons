<?php
class User {

	public $firstname;
	public $lastname;

	function __construct($firstname, $lastname) {
		$this->firstname = $firstname;
		$this->lastname = $lastname;
	}

	public function __toString ()
	{
		return "Пользователь: <b>{$this->firstname} {$this->lastname}</b>";
	}
}

$user = new User('Владимир', 'Бех');
echo $user;
