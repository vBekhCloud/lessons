<?php
class Product {
	public static $company = 'ООО Аспро';

	public $price = 10000;
	public $name;

	function __construct($name, $price) {
		$this->name = $name;
		$this->price = $price;

		print "!!Конструктор класса Product!! <br>";
	}

	static function display_company() {
		echo self::$company;
	}

	function get_info() {
		return [
			'name' => $this->name,
			'price' => $this->price,
		];
	}

	function get_label() {
		return "Решение: {$this->name}. Цена - {$this->price}";
	}
}

$company = Product::$company;
echo "Решения от компании {$company}:";
echo '<br>';

$product = new Product('Prioity', 30000);
echo $product->get_label();

echo '<br>';
