<?php
$directory_name = basename(__DIR__);
$files = scandir(__DIR__);
$excluded_names = ['.', '..', 'index.php'];

$lessons_files = array_diff($files, $excluded_names);
?>

<?foreach($lessons_files as $filename):?>
	<div>
		<a href="/<?=$directory_name?>/<?=$filename?>">
			<?=$filename?>
		</a>
	</div>
<?endforeach;?>