<?php
class User {

	public $firstname;
	public $lastname;

	function __construct($firstname, $lastname) {
		$this->firstname = $firstname;
		$this->lastname = $lastname;
	}

	public function __get ($name)
	{
		echo "__get";
		echo '<br>';

		switch($name) {
			case 'full_name':
				return "{$this->firstname} {$this->lastname}";
		}
	}

	public function __set ($name, $value)
	{
		echo "__set";
		echo '<br>';

		switch($name) {
			case 'full_name':
				[$firstname, $lastname] = explode(' ', $value);
				$this->firstname = $firstname;
				$this->lastname = $lastname;
				break;
		}
	}
}

$user = new User('Арнольд', 'Эрнст');
$user->full_name = 'Владимир Бех';
echo $user->full_name;
