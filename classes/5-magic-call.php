<?php
class User {

	public $firstname;
	public $lastname;

	public static $type = 'human';

	function __construct($firstname, $lastname) {
		$this->firstname = $firstname;
		$this->lastname = $lastname;
	}

	public function __call ($name, $arguments)
	{
		echo "__call";
		echo '<br>';

		switch($name) {
			case 'display_greetings':
				echo "Здравствуйте {$this->firstname} {$this->lastname}!";
				echo '<br>';
				break;
		}
	}

	public static function __callStatic ($name, $arguments)
	{
		echo "__callStatic";
		echo '<br>';

		switch($name) {
			case 'display_object_type':
				$type = self::$type;
				echo "Объект типа {$type}";
				echo '<br>';
				break;
		}
	}

//	function display_greetings() {
//		echo "Здравствуйте {$this->firstname} {$this->lastname}!";
//	}
}

User::display_object_type();

$user = new User('Владимир', 'Бех');
$user->display_greetings();
