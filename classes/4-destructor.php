<?php
class DataBase {

	public $host = '';
	public $username = 'root';
	public $password = '';
	public $database = '';

	function __construct($host, $username, $password, $database) {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;

		$this->connect();
	}

	function __destruct() {
		$this->disconnect();
	}

	function execute() {
		echo "Выполняем запросы...";
		echo '<br>';
	}

	function connect() {
		print "Подключаемся к БД <br>";
	}

	function disconnect() {
		print "Отключаемся от БД <br>";
	}
}

$connection = new DataBase('localhost:3036', 'root', '', 'myDB');

$connection->execute();
$connection->execute();

//$connection = null;
//unset($connection);
//$connection = 'test';

$connection->execute();
