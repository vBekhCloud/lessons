<?php
class SimpleClass {
	// объявление свойства
	public $var = 'значение по умолчанию';

	// объявление метода
	function display_var() {
		echo $this->var;
	}
}

$object = new SimpleClass();
$object->display_var();
