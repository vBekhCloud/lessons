<?php
class Product {
	// объявление свойства
	public static $company = 'ООО Аспро';
	public static $price = 50000;

	public $name = 'Максимум';

	// объявление метода
	function display_company() {
		echo self::$company;
	}

	static function display_price() {
		return self::$price;
	}
}

$object = new Product();
$object->display_company();

echo '<br>';

echo Product::$company;

echo '<br>';

echo Product::display_price();