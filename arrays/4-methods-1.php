<?php
$animals = [
	[
		'name' => 'Sharik',
		'age' => 4.5,
		'type' => 'dog',
	],
	[
		'name' => 'Tuzik',
		'age' => 2,
		'type' => 'dog',
	],
	[
		'name' => 'Jack',
		'age' => 8.1,
		'type' => 'dog',
	],
	[
		'name' => 'Busya',
		'age' => 3,
		'type' => 'cat',
	],
	[
		'name' => 'Murka',
		'age' => 12,
		'type' => 'cat',
	],
];
?>

<pre>
<?php
print_r($animals);
?>
</pre>

<br>
<br>

Получить клички всех животных
<br>
<br>
array_column($animals, 'name')
<br>
<br>
<?php
print_r( array_column($animals, 'name') );
?>

<br>
<br>
<br>
<br>

Получить возраст самого старого животного
<br>
<br>
max( array_column($animals, 'age') )
<br>
<br>
<?php
print_r( max( array_column($animals, 'age') ) );
?>

<br>
<br>
<br>
<br>

Получить типы животных и их количество
<br>
<br>
array_count_values( array_column($animals, 'type') )
<br>
<br>
<?php
print_r( array_count_values( array_column($animals, 'type') ) );
?>
