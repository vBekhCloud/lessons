$multilevel_array = [
[
'one',
'two',
'three',
],
[
1,
2,
3,
],
[
true,
false,
true,
],
];
<br>
<?php
$multilevel_array = [
	[
		'one',
		'two',
		'three',
	],
	[
		1,
		2,
		3,
	],
	[
		true,
		[
			'third level!!'
		],
		'dog' => [
			'name' => 'Sharik',
			'age' => 4.5
		],
	],
];
?>

<pre>
<?php
print_r($multilevel_array);
?>
</pre>

<br>
echo $multilevel_array[1][0];
<br>
<?php
echo $multilevel_array[1][0];