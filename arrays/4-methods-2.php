<?php
$users = [
	[
		'id' => '23',
		'name' => 'Вася',
		'age' => 23,
		'position' => 'employee',
	],
	[
		'id' => '32',
		'name' => 'Петя',
		'age' => 34,
		'position' => 'employee',
	],
	[
		'id' => '546',
		'name' => 'Коля',
		'age' => 25,
		'position' => 'head',
	],
];
?>

<pre>
<?php
print_r($users);
?>
</pre>

<?php
$users_2 = [
	[
        'id' => '234',
		'name' => 'Дима',
		'age' => 42,
		'position' => 'head',
	],
	[
        'id' => '555',
		'name' => 'Аня',
		'age' => 34,
		'position' => 'employee',
	],
	[
		'id' => '768',
		'name' => 'Сергей',
		'age' => 41,
		'position' => 'employee',
	],
];
?>

<pre>
<?php
print_r($users_2);
?>
</pre>

<br>
<br>

Получить список всех сотрудников
<br>
<br>
array_merge($users, $users_2)
<br>
<br>
<pre>
<?php
print_r( array_merge($users, $users_2) );
?>
</pre>

<br>
<br>

Значения каждого следующего массива перетирают значения предыдущего
<br>
Для ассоциативных массивов
<br>
<br>
<pre>
<?php
print_r( $array1 = [
    'key_1' => 'value_1',
    'key_2' => 'value_2',
] );
print_r( $array2 = [
    'key_1' => 'different',
    'key_3' => 'something new',
] );
?>
</pre>
array_merge($array1, $array2)
<br>
<br>
<pre>
<?php
print_r( array_merge($array1, $array2) );
?>
</pre>

<br>
<br>

Добавить сотрудника в конец массива
<br>
<br>
array_push($users_2, [
<br>
'id' => '323',
<br>
'name' => 'Виктор',
<br>
'age' => '21',
<br>
'position' => 'employee',
<br>
]);
<br>
<br>
$users_2[] = [
<br>
'id' => '323',
<br>
'name' => 'Виктор',
<br>
'age' => '21',
<br>
'position' => 'employee',
<br>
];
<br>
<br>
<pre>
<?php
array_push($users_2, [
	'id' => '323',
	'name' => 'Виктор',
	'age' => '21',
	'position' => 'employee',
]);
print_r($users_2);
?>
</pre>

<br>
<br>

Добавить сотрудника в начало массива
<br>
<br>
array_unshift($users_2, [
<br>
'id' => '111',
<br>
'name' => 'Валентин',
<br>
'age' => '22',
<br>
'position' => 'employee',
<br>
]);
<br>
<br>
<pre>
<?php
array_unshift($users_2, [
	'id' => '111',
	'name' => 'Валентин',
	'age' => '22',
	'position' => 'employee',
]);
print_r($users_2);
?>
</pre>