<?php
$users = [
	'623' => [
		'id' => '623',
		'name' => 'Вася',
		'age' => 15,
		'position' => 'employee',
	],
	'32' => [
		'id' => '32',
		'name' => 'Петя',
		'age' => 34,
		'position' => 'employee',
	],
	'546' => [
		'id' => '546',
		'name' => 'Коля',
		'age' => 25,
		'position' => 'head',
	],
];
?>

<pre>
<?php
print_r($users);
?>
</pre>

<br>
<br>

array_keys($users);
<br>
<pre>
<?php
print_r( array_keys ($users) );
?>
</pre>

<br>
<br>

array_filter
<br>
<pre>
<?php
$low_board_age = 18;
print_r( array_filter($users, function($user) {
    return true;
}) );
?>
</pre>

<br>
<br>

array_map
<br>
<pre>
<?php
$older_employee_age = 30;
print_r(
    array_map(
        function($user) {
//            if() {
//                 = 'older employee';
//            }
            return $user;
        },
        $users
    )
);
?>
</pre>

<br>
<br>

sort
<br>
<pre>
<?php
//sort($users);
//print_r($users);
?>
</pre>

<br>
<br>

usort
<br>
<pre>
<?php
//usort($users, function($user_1, $user_2) {
//    return $user_1['age'] > $user_2['age'];
//});
//print_r($users);
?>
</pre>