$simple_array = [
'one',
'two',
'three',
];
<br>
<?php
$simple_array = [
	'one',
	'two',
	'three',
];
?>

<pre>
<?php
print_r($simple_array);
?>
</pre>


$combined_array = [
'one',
2,
false
];
<br>
<?php
$combined_array = [
	'one',
	2,
	false,
];
?>


<?php
var_dump($combined_array);
?>