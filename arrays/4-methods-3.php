<?php
$animals = [
	'кошка',
    'собака',
    'мышь',
	'бегемот',
];

$animals_2 = [
	'кошка',
	'мышь',
    'жираф',

];
?>

<pre>
<?php
print_r($animals);
?>

<?php
print_r($animals_2);
?>
</pre>

<br>
<br>

array_diff
<br>
<pre>
<?php
print_r( array_diff($animals, $animals_2) );
print_r( array_diff($animals_2, $animals) );
?>
</pre>

<br>
<br>

array_intersect
<br>
<pre>
<?php
print_r( array_intersect($animals, $animals_2) );
?>
</pre>

<br>
<br>

reset
<br>
<pre>
<?php
print_r( reset($animals) );
?>
</pre>

<br>
<br>

end
<br>
<pre>
<?php
print_r( end($animals) );
?>
</pre>

<?php
$animals_3 = [
	'кошка',
	'мышь',
	'кошка',
	'мышь',
	'жираф',
	'кошка',
];
?>

<br>
<br>

<pre>
<?php
print_r($animals_3);
?>
</pre>

<br>
<br>

array_unique
<br>
<pre>
<?php
print_r( array_unique($animals_3) );
?>
</pre>