Примеры использования list()

<pre>
<?php
$info = array('кофе', 'коричневый', 'кофеин');
print_r($info);
?>
</pre>

<br>
list($drink, $color, $power) = $info;
<br>
echo "$drink - $color, а $power делает его особенным.\n";
<br>
<br>
<?php
// Составить список всех переменных
list($drink, $color, $power) = $info;
echo "$drink - $color, а $power делает его особенным.\n";
?>
<br>
<br>
<br>
list($drink, , $power) = $info;
<br>
echo "В $drink есть $power.\n";
<br>
<br>
<?php
// Составить список только некоторых из них
list($drink, , $power) = $info;
echo "В $drink есть $power.\n";
?>
<br>
<br>
<br>
list( , , $power) = $info;
<br>
echo "Мне нужен $power!\n";
<br>
<br>
<?php
// Или пропустить все, кроме третьей
list( , , $power) = $info;
echo "Мне нужен $power!\n";
?>
<br>
<br>
<br>
list($bar) = "abcde";
<br>
var_dump($bar);
<br>
<br>
<?php
// list() не работает со строками
list($bar) = "abcde";
var_dump($bar);
?>
<br>
<br>

<br>
<br>
<br>
list($a, list($b, $c)) = array(1, array(2, 3));
<br>
var_dump($a, $b, $c);
<br>
<br>
<?php
list($a, list($b, $c)) = array(1, array(2, 3));
var_dump($a, $b, $c);
?>

<br>
<br>
<br>
<br>

Порядок, в котором индексы массива будут использоваться функцией list(), не имеет значения.
<br>

<pre>
<?php
$foo = array(2 => 'a', 'foo' => 'b', 0 => 'c');
print_r($foo);
?>
</pre>

$foo[1] = 'd';
<br>
list($x, $y, $z) = $foo;
<br>
var_dump($x, $y, $z);
<br>
<br>

<?php
$foo[1] = 'd';
list($x, $y, $z) = $foo;
var_dump($x, $y, $z);
?>

<br>
<br>
<br>
<br>

Пример list() с ключами
<br>
<br>
Начиная с PHP 7.1.0, для list() можно задавать конкретные ключи, которые могут быть произвольными выражениями.
<br>
Допустимо смешивать строковые и числовые ключи.
<br>
Однако элементы с ключами и без ключей не могут быть использоваться одновременно.
<br>

<pre>
<?php
$data = [
    ["id" => 1, "name" => 'Tom'],
    ["id" => 2, "name" => 'Fred'],
];
print_r($data);
?>
</pre>

foreach ($data as ["id" => $id, "name" => $name]) {
<br>
echo "id: $id, name: $name\n";
<br>
}
<br>
<br>
<?php
foreach ($data as ["id" => $id, "name" => $name]) {
    echo "id: $id, name: $name\n";
}
?>
<br>
<br>
<br>
<br>

list(1 => $second, 3 => $fourth) = [1, 2, 3, 4];
<br>
echo "$second, $fourth\n";
<br>

<?php
list(1 => $second, 3 => $fourth) = [1, 2, 3, 4];
echo "$second, $fourth\n";